# # Navadne diferencialne enačbe
# 
# Rešujemo začetni problem na diferencialno enačbo
#  $$ y'(t) = f(t, y)$$
# z začetnim pogojem $y(t_0) = y_0$. Funkcija $f(t, y)$, $t_0$ in $y_0$ so dani, iščemo pa približek
# za funkcijo $y(t)$ na intervalu $[t_0, t_k]$ za dani končni parameter $t_k$.  

"""
    t, y = euler(fun, t0, y0, tk, n)

Poišči približek za rešitev diferencialne enačbe `y'(t)=f(t, y(t))` z začetnim pogojem `y(t0) = y0` na intervalu
`[t0, tk]` z n koraki Eulerjeve metode.
"""
function euler(fun, t0, y0, tk, n)
    t = LinRange(t0, tk, n+1)
    h = t[2] - t[1]
    y = zeros(n+1)
    y[1] = y0
    for i=1:n
        y[i+1] = y[i] + h * fun(t[i], y[i])
    end
    t, y
end

logistic(t,y) = y*(1-y)
t0 = 0
y0 = 0.1

t, y = euler(logistic, t0, y0, 5, 10)

using Plots

p = plot(t, y)

"""
    t, y = implicit_trapez(fun, t0, y0, tk, n)

Poišči približek za rešitev diferencialne enačbe `y'(t)=f(t, y(t))` z začetnim pogojem `y(t0) = y0` na intervalu
`[t0, tk]` z n koraki implicitne trapezne metode.
"""
function implicit_trapez(fun, t0, y0, tk, n)
    t = LinRange(t0, tk, n+1)
    h = t[2] - t[1]
    y = zeros(n+1)
    y[1] = y0
    for i=1:n
        y[i+1] = y[i] + h * fun(t[i], y[i]) # začetni približek za y[i+1]
        for j = 1:1
            y[i+1] = y[i] + h/2 * (fun(t[i], y[i]) + fun(t[i+1], y[i+1])) # navadna iteracija
        end
    end
    t, y
end
