# # Primer uporabe avtomatskega odvajanja
# 
# [Podatke o potrjenih okužbah s COVID-19 virusom](https://github.com/owid/covid-19-data) želimo 
# aproksimirati z [logistično krivuljo](https://en.wikipedia.org/wiki/Logistic_function):
#    $$ l(t; N, k, t_0) = \frac{N}{1 + e^{-k(t-t_0)}}. $$
# Želimo poiskati vrednosti parametrov $N_0$, $k$ in $t_0$, tako da bo razlika med napovedjo z 
# logistično funkcijo in dejanskimi podatki čim manjša. 

"""
    y = logistic(t, N, k, t0)

Izračunaj vrednost logistične funkcije.
"""
function logistic(t, N, k, t0)
    return N/(1 + exp(-k*(t - t0)))
end

using Plots

plot(t -> logistic(t, 1, 1, 0), -3, 5; title="Logistična krivulja")

# Podatke dobimo kot csv datoteko
import CSV
using DataFrames

data = CSV.read("./vaje/8_odvod/total_cases.csv", DataFrame)
slo_data = dropmissing(data[:, [:Slovenia]])
slo_data[1:100, :Slovenia]

scatter(slo_data[1:60, :Slovenia], label="Slovenija")

plot!(t->logistic(t, 1400, 0.13, 30), -10, 60)

# Želimo poiskati parametre $N$, $k$ in $t_0$, da se bo krivulja kar se da prilegala
# podatkom. Prileganje lahko ocenimo s funkcijo napake
#   $$err(t, y; N, k, t_0)= \sum_{i} \left(y_i - l(t_i)\right).$$
# Iščemo parametre, pri katerih je napaka najmanjša. Iščemo torej minimum funkcije $err(t, y; N, k, t_0)$ 
# pri danih vrednostih $t$ in $y$. 

"""
    e = error(t, y, N, k, t_0)

Izračunaj kvadrat razlik med podatki in napovedjo logističnega modela. 
"""
function error(t, y, N, k, t0)
    e = 0
    for i=1:length(t)
        e += (y[i] - logistic(t[i], N, k, t0))^2
    end
    return e
end

t = 1:50
y = slo_data[1:50, :Slovenia]
error(t, y, 1e3, 0.13, 29)

# ## Gradientna metoda
# 
# Minimom funkcije poiščemo z gradientno metodo. Zato potrebujemo gradient funkcije $err$ po 
# parametrih $N$, $k$ in $t_0$. Uporabimo avtomatski odvod. 

# Najprej odvajamo funkcije "na roke".

"""
    y, grad = logistic_grad(t, N, k, t0)

Izračunaj vrednost logistične funkcije in gradient po parametre.
"""
function logistic_grad(t, N, k, t0)
    y = N/(1 + exp(-k*(t - t0)))
    dy_po_dN = 1/(1 + exp(-k*(t - t0)))
    dy_po_dk = -N/(1 + exp(-k*(t - t0)))^2*exp(-k*(t - t0))*(-(t - t0))
    dy_po_dt0 = -N/(1 + exp(-k*(t - t0)))^2*exp(-k*(t - t0))*k
    return y, [dy_po_dN, dy_po_dk, dy_po_dt0]
end

"""
    e, grad = error_grad(t, y, N, k, t_0)

Izračunaj kvadrat razlik med podatki in napovedjo logističnega modela. Poleg vrednosti napake funkcija izračuna tudi gradient po 
parametrih N, k in t_0.
"""
function error_grad(t, y, N, k, t0)
    e = 0
    grad_e = zeros(3)
    for i=1:length(t)
        yl, grad_l = logistic_grad(t[i], N, k, t0)
        e += (yl - y[i])^2
        grad_e += 2*(yl - y[i])*grad_l
    end
    return e, grad_e
end

using LinearAlgebra

contour(LinRange(0.1, 0.2, 30), LinRange(20, 40, 30), (k, t0)->error(t, y, 1400, k, t0))

p = [1400, 0.13, 30]
scatter!([p[2]], [p[3]])
e, grad = error_grad(t, y, p[1], p[2], p[3])
p = p - 0.01*grad/norm(grad)

using NumMat

N = Dual(1400, [1, 0, 0])
k = Dual(0.13, [0, 1, 0])
t0 = Dual(30, [0, 0, 1])

err = error(t, y, N, k, t0) 

# ## Gauss Newtonova metoda
# 
# Za iskanje minima funkcije napake je bolje uporabiti Gauss-Newtonovo
# metodo. Pri tej 

function residuals(t, y, N, k, t0)
    [logistic(ti, N, k, t0) for ti in t] - y
end

function residuals_grad(t, y, N, k, t0)
    n = length(y)
    jacobian = zeros(n, 3)
    res = zeros(n)
    for i=1:n
        res[i], jacobian[i,:] = logistic_grad(t[i], N, k, t0)
        res[i] -= y[i]
    end
    return res, jacobian
end

contour(LinRange(0.1, 0.2, 30), LinRange(20, 40, 30), (k, t0)->error(t, y, 1400, k, t0))

p = [1400; 0.13; 30]
scatter!([p[2]], [p[3]])
res, jacobian = residuals_grad(t, y, p[1], p[2], p[3])
p = p - jacobian\res

# ## Uporaba dinamičnega modela
# 
# Logistična funkcija je zelo omejena. Obstajajo [boljši modeli]([](https://en.wikipedia.org/wiki/Compartmental_models_in_epidemiology), 
# ki opisujejo širjenje okužbe. 
# Ogledali si bomo, kako bi določili parametre [SIR modela](https://en.wikipedia.org/wiki/Compartmental_models_in_epidemiology#The_SIR_model),
# ki najbolje ustrezajo danim podatkom. V primeru modela SIR nimamo eksplicitnih formul za izračun napovedi, 
# ampak moramo širjenje simulirati z metodami za reševanje navadnih diferencialnih enačb. 
# Tako ni čisto jasno, kako parameteri modela vplivajo na končno rešitev. V primeru, ko smo kot model
# uporabili logistično krivuljo, bi lahko na roke izračunali odvod po parametrih. V primeru
# dinamičnega modela pa ni več tako preprosto, saj parametri nastopajo implicitno. Tu nam priskoči na pomoč 
# avtomatski odvod, saj omogoča izračun odvoda poljubnega računalniškega programa.

function sir(S, I, β, N, γ)
    [-β*I*S/N, β*I*S/N - γ*I]
end

function euler(f, t0, y0, tk, n)
    y = [y0,]
    t = LinRange(t0, tk, n+1)
    h = t[2] - t[1]
    for i=1:n
        push!(y, y[i] + h*f(t[i], y[i]))
    end
    return t, y
end

function residuals_sir(t, y, β, N, γ)
    f = (t, SI) -> sir(SI[1], SI[2], β, N, γ)
    SI0 = [N-y[1], y[1]] 
    t, ym = euler(f, t[1], SI0, t[end], length(t) - 1)
    res = y[2:end] - [ym_k[2] for ym_k in ym[2:end]]
    return res
end

residuals_sir(t, y, 1., 1400., 1.)

β = Dual(1.0, [1., 0, 0])
N = Dual(1400., [0, 1., 0])
γ = Dual(1.0, [0, 0, 1.])

res_dual = residuals_sir(t, y, β, N, γ)

jacobian = reduce(hcat, [res_k.dx for res_k in res_dual])'
res = [res_k.x for res_k in res_dual]

p = [1.0, 1400., 1.0]
p = p - jacobian\res 