# # Numerično odvajanje s končnimi diferencami

num_diff(f, x, h) = (f(x+h/2) - f(x-h/2)) / h

num_diff(sin, 0, 10.0^(-7))

"""
    h, napaka = napaka_diff(f, df, x)

Izračunaj napako pri računanju odvoda s končnimi diferencami za različne vrednosti h
"""
function napaka_diff(f, df, x)
    h = [2.0^i for i in -1:-1:-50]
    prava = df(x)
    napaka = [(num_diff(f, x, hi) - prava) for hi in h]
    return h, napaka
end

using Plots

h, napaka = napaka_diff(sin, cos, 1)

scatter(h, abs.(napaka), label=false, xaxis=:log, yaxis=:log)
savefig("napaka_odvod.png")